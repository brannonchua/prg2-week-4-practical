﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRG2_Week_4_Practical
{
    class SavingsAccount : BankAccount
    {
        private double rate;

        public double Rate
        {
            get { return rate; }
            set { rate = value; }

        }

        public SavingsAccount()
        {

        }

        public SavingsAccount(string accNo,string accName, double balance, double rate) : base(accNo, accName, balance)
        {
            Rate = rate;
        }

        public double CalculateInterest()
        {
            return (Balance * Rate);

        }

        public string ToString()
        {
            return base.ToString() + "Rate: " + rate;
        }
    }
}
