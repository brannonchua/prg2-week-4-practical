﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace PRG2_Week_4_Practical
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        SavingsAccount account1 = new SavingsAccount();
        SavingsAccount account2 = new SavingsAccount();
        List<SavingsAccount> bank = new List<SavingsAccount>();
        public void InitAccount()
        {
            account1 = new SavingsAccount("111-001", "Bob", 2000.00, 0.02);
            account2 = new SavingsAccount("111-002", "Mary", 5000.00, 0.02);
        }
        public MainPage()
        {
            this.InitializeComponent();
            InitAccount();
            account1Txt.Text = string.Format("{0}", account1.ToString());
            account2Txt.Text = string.Format("{0}", account2.ToString());
        }

        private void account1DepositBtn_Click(object sender, RoutedEventArgs e)
        {
            account1.Deposit(1000);
            account1Txt.Text = account1.ToString();

        }

        private void account1WithdrawBtn_Click(object sender, RoutedEventArgs e)
        {
            if (account1.Withdraw(1000) == true)
            {
                account1Txt.Text = account1.ToString();
                if (account1.Balance == 0)
                {
                    account1Txt.Text = ("The balance is insufficient");
                }
            }
            else
            {
                account1Txt.Text = ("The balance is insufficient");
            }
        }

        private void account1InterestBtn_Click(object sender, RoutedEventArgs e)
        {
            account1.CalculateInterest();
            account1Txt.Text = ("Interest: " + account1.CalculateInterest());
        }

        private void account2InterestBtn_Click(object sender, RoutedEventArgs e)
        {
            account2.CalculateInterest();
            account2Txt.Text = ("Interest: " + account1.CalculateInterest());
            
        }

        private void account2WithdrawBtn_Click(object sender, RoutedEventArgs e)
        {
            if (account2.Withdraw(1000) == true)
            {
                account2Txt.Text = account2.ToString();
                if (account2.Balance == 0)
                {
                    account2Txt.Text = ("The balance is insufficient");
                }
            }
            else
            {
                account2Txt.Text = ("The balance is insufficient");
            }
        }

        private void account2DepositBtn_Click(object sender, RoutedEventArgs e)
        {
            account2.Deposit(1000);
            account2Txt.Text = account2.ToString();
        }
    }
}
