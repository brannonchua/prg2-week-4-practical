﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRG2_Week_4_Practical
{
    class BankAccount
    {
        private string accNo;
        public string AccNo
        {
            get { return accNo; }
            set { accNo = value; }
        }

        private string accName;
        public string AccName
        {
            get { return accName; }
            set { accName = value; }
        }

        private double balance;
        public double Balance
        {
            get { return balance; }
            set { balance = value; }
        }

        public BankAccount()
        {

        }

        public BankAccount(string accNo,string accName,double balance)
        {
            AccNo = accNo;
            AccName = accName;
            Balance = balance;
        }

        public void Deposit(double amount)
        {
            balance = balance + amount;
        }

        public bool Withdraw(double amount)
        {
            if (amount <= balance)
            {
                balance = balance - amount;
                return true;
            }
            else
            {
                return false;
            }
        }

        public double EnquireBalance()
        {
            return balance;
        }

        public string ToString()
        {
            return ( "accNo: " + AccNo + Environment.NewLine + "accName: " +
                    AccName + Environment.NewLine + "Balance: " + balance + Environment.NewLine);
        }
    }
}
